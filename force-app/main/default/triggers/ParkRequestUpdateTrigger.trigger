trigger ParkRequestUpdateTrigger on ParkRequestUpdate__c (after insert) {
    if (Trigger.isInsert) {
        if (Trigger.isAfter) {
            updateParkRequest();
        }
    }

    private static void updateParkRequest() {
        List<ParkRequest__c> parkRequestsToUpdate = new List<ParkRequest__c>();
        for (ParkRequestUpdate__c u : Trigger.new) {
            parkRequestsToUpdate.add(
                new ParkRequest__c(
                    Id = u.ParkRequest__c,
                    ParkAllocated__c = u.ParkAllocated__c,
                    Requested__c = u.Requested__c,
                    SuperPark__c = u.SuperPark__c
                )
            );
        }

        update parkRequestsToUpdate;

        ParkingAllocator.determineParkers(pr.Date__c);
    }
}